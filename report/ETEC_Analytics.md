**Opracowanie i przygotowanie wizualizacji danych będących podstawą aplikacji**
**internetowej służącej do analizy wyników badań PISA i TIMSS**

------

Table of Content

[toc]



<div style="page-break-after: always"></div>


## Introduction 


>
> This reports documents implementation process of Educational Data Analytics - an interactive dashboard for international education assessments. The former allow users to explore i) mean results from TIMSS and PISA assessments along with additional students characteristics  on chosen levels of data aggregation, ii) correlations between variables. Modules are deployed online. Module 1 runs on Gitlab Pages and module 2 on Herokuapps. 

# Module 1: Means 

Module 1 provides a drag'n'drop dashboard for exploration of TIMSS and PISA results. Update with a use of data from new editions of these assessment is possible and straightforward. In the following section we describe the data pipeline applied in the module. 

<div style="page-break-after: always"></div>



## Module structure

Fig. 1. An overview of the Module 1 structure.

![](./img/project_structure.png)

# ① Raw Data

Input data should be provided in the Stata format (.dta). However, our script can be easily modified to accept other popular data formats (e.g. spss, csv, pickle, xls).

# ② Data Analysis

Data Analysis can be carried out either using Stata or Python. We provide both scripts in the [project’s repository](https://gitlab.com/etecsa/dash-dev). Main output of the analysis are students scores taking into account PVs and weights (see e.g.: [link](https://nces.ed.gov/timss/timss15technotes_weighting.asp)). Output csv file is delivered in the long format. 

In the current state all data analyses are performed in the standalone scripts (Python or Stata). These scripts require fine tuning for specific assessments and local environments, i.e. changing local paths, adjusting code for specific assessment edition, variables of interest etc. In the future we plan to automate this process and provide users a dedicated software in a form of an open source Python library. 

In the following section we present a documented Python analysis covered in the Jupyter notebook format in the file: 

`0_timss_means.ipynb`

In this notebook we: 

- read data from the latest edition of TIMSS (2019) 
- tranform it to the format suitable for working in Python
- perform basic exploratory analysis
- visualize the data

### Load necessary packages for data wrangling, analysis and visualization

```python
import numpy as np
import pandas as pd
from statsmodels.stats.weightstats import DescrStatsW
import matplotlib.pyplot as plt
import seaborn as sns
import plotly.graph_objects as go
import os
import glob
```

### I Reading and cleaning the data

```python
# Throughout this analysis we rely on TIMSS data in STATA format (.dta) provided by Evidence Institut
# Please define your local path to the required input STATA files
path='./in/2019/'

# Grade 4 student context data files. Please note that this is a >300MB file, so it loads a while
df=pd.read_stata(path+'T2019_asg_gr4.dta
                 
# We need to merge our df with a codebook including country names
idcntry=pd.read_csv(path+'idcntry.csv', sep=';')
df=df.merge(idcntry,on='idcntry')
                 
# Create lists for two sets of plausible values (PV) - for math and science results accordingly
cols_math = [c for c in df.columns if c.lower()[:6] == 'asmmat']
cols_science = [c for c in df.columns if c.lower()[:6] == 'asssci']
                 
# Pandas didn't recognize correct datatypes from the dta file. We need to transform it to numeric format
df[cols_math+cols_science+['totwgt']] = df[cols_math+cols_science+['totwgt']].apply(pd.to_numeric, errors='coerce', axis=1)

# Create a smaller dataframe consisting of relevant PV columns, country identifier (idcntry) and weights column (totwgt)
t=df[cols_math+cols_science+['totwgt','idcntry','country','iso']]

# Exclude some irrelevant data
t=t[t['idcntry']<1000]                 
```

### II Calculate weighted means

```python
# Function for calculating weighted averages. It uses a defined list of columns (`cols`) and pandas series of weights (column `totwgt`). 
# We group our df by columns defined in gr_cols and apply a numpy weighted average function
gr_cols = ['idcntry','iso','country']
wtavg = lambda x: np.average(x[cols], weights = x['totwgt'], axis = 0)
# math
cols=cols_math
dfwavg_m=t.groupby(gr_cols).apply(wtavg)
# science
cols=cols_science
dfwavg_s=t.groupby(gr_cols).apply(wtavg)

# For each country we get a list of PV values. dfwavg_m contains PVs for math, and dfwavg_s for science
dfwavg_m.head()
```

```python
idcntry  iso  country   
8        ALB  Albania       [493.32181607236004, 493.47041208252733, 494.7...
31       AZE  Azerbaijan    [516.1452084017347, 515.3661670069741, 515.846...
36       AUS  Australia     [515.8887109485572, 516.1832434232251, 516.437...
40       AUT  Austria       [538.8396906048005, 539.6907286541078, 539.703...
48       BHR  Bahrain       [480.2182878714722, 480.3534745152164, 481.005...
dtype: object
```

```python
# Now we can calculate mean of 5 PVs for all the countries and for both subjects
means_math=[]
means_science=[]
for l in dfwavg_m:
    means_math.append(l.mean())
for l in dfwavg_s:
    means_science.append(l.mean())
tdf=pd.DataFrame(dfwavg_m)
tdf['means_math']=means_math
tdf['means_science']=means_science

# Data cleaning
tdf.reset_index(inplace=True)
tdf=tdf.drop([0],axis=1)

# Show first 5 rows
tdf.head()
```

|      | idcntry | iso  | country    | means_math | means_science |
| ---- | ------- | ---- | ---------- | ---------- | ------------- |
| 0    | 8       | ALB  | Albania    | 494.017496 | 489.480246    |
| 1    | 31      | AZE  | Azerbaijan | 515.454554 | 426.734741    |
| 2    | 36      | AUS  | Australia  | 515.880111 | 532.574948    |
| 3    | 40      | AUT  | Austria    | 539.219316 | 522.060505    |
| 4    | 48      | BHR  | Bahrain    | 479.852664 | 492.542474    |

```python
tdf_m=tdf[tdf['itsex']=='Male']
tdf_f=tdf[tdf['itsex']=='Female']

# We need data in the long format for visualization purposes
df_m=pd.wide_to_long(tdf_m, stubnames=['means'], i='country',j='subject', sep='_',suffix='\w+')
df_f=pd.wide_to_long(tdf_f, stubnames=['means'], i='country',j='subject', sep='_',suffix='\w+')

df_m.reset_index(inplace=True)
df_f.reset_index(inplace=True)

tdf=pd.concat([df_m,df_f])

tdf.rename(columns={'means':'score'},inplace=True)

# Our basic dataset is ready!
tdf.to_csv('./out/timss_means.csv',index=False)
```

### Basic visualizations

```python
# Choose countries for the means plot
cntrlist=['USA','SAU','POL','QAT','PHL']
dfplt=tdf[tdf['iso'].isin(cntrlist)] 
dfplt=dfplt.sort_values('means_math')

# List of country ISO codes
tdf.iso.unique()
```

```python
array(['ALB', 'AZE', 'AUS', 'AUT', 'BHR', 'ARM', 'BIH', 'BGR', 'CAN',
       'CHL', 'TWN', 'HRV', 'CYP', 'CZE', 'DNK', 'FIN', 'FRA', 'GEO',
       'DEU', 'SAR', 'HUN', 'IRN', 'IRL', 'ITA', 'JPN', 'KAZ', 'KOR',
       'XKX', 'KWT', 'LVA', 'LTU', 'MLT', 'MNE', 'MAR', 'OMN', 'NLD',
       'NZL', 'NOR', 'PAK', 'PHL', 'POL', 'PRT', 'QAT', 'RUS', 'SAU',
       'SRB', 'SGP', 'SVK', 'ZAF', 'ESP', 'SWE', 'ARE', 'TUR', 'MKD',
       'USA', 'ENG', 'BFL'], dtype=object)
```

```python
sns.set_theme(style="whitegrid")

ax = sns.barplot(x="country", y="means_math", data=dfplt)

# Uncomment the next line to save plot as png file
# plt.savefig('means_plot.png')
```

<img src="./img/means.png" width="300" align=center />

```python
# To compare math and science results we need to reshape our df using melt function (seaborn needs the data in the long format)
dfplt=dfplt[['iso','means_math','means_science']]
dfplt = pd.melt(dfplt, id_vars="iso", var_name="subject", value_name="result")

sns.catplot(x='iso', y='result', hue='subject', data=dfplt, kind='bar')

# Uncomment the next line to save plot as png file
# plt.savefig('math_vs_science_plot.png')
```

<img src="./img/means_cat.png" width="300" align=center />

### Percentiles and gender

In the following part of the notebook we provide instructions on creating a similar output file as basic `timss_means.csv` but this time using percentiles.

```python
# Unfortunately numpy doesn't provide a function for calculating weighted percentiles.
# We can write our own function or rely on an another Python package: statsmodels. We chose the latter option.
wtqt = lambda x: DescrStatsW(x[cols], x['totwgt'])
cols=cols_math
# Create a list of percentiles
perc=list(np.linspace(0,1,11))
perc=perc[1:10]
# We apply the wtqt function on the range of columns and calculate weighted percentiles
dfwtqt_m=t.groupby(gr_cols).apply(wtqt)
dfqt=pd.DataFrame(dfwtqt_m)
for p in perc:
    qt=[]
    for l in dfwtqt_m:
        qt.append(l.quantile(probs=np.array(p), return_pandas=False).mean())
    dfqt['p'+str(p*100)[:2]+'_math']=qt

# Data cleaning
dfqt.reset_index(inplace=True)
dfqt=dfqt.drop([0],axis=1)    

# View 5 first rows
dfqt.head()
```

|      | idcntry | iso  | country    | p10_math   | p20_math   | p30_math   | p40_math   | p50_math   | p60_math   | p70_math   | p80_math   | p90_math   |
| ---- | ------- | ---- | ---------- | ---------- | ---------- | ---------- | ---------- | ---------- | ---------- | ---------- | ---------- | ---------- |
| 0    | 8       | ALB  | Albania    | 382.062766 | 425.008952 | 454.972050 | 478.788760 | 499.815842 | 519.245030 | 540.459996 | 565.119924 | 597.402294 |
| 1    | 31      | AZE  | Azerbaijan | 412.009474 | 452.691312 | 478.870554 | 501.304732 | 520.969320 | 540.927122 | 562.263874 | 585.742594 | 615.497518 |
| 2    | 36      | AUS  | Australia  | 401.019734 | 444.686538 | 473.665228 | 497.590622 | 518.961026 | 540.353050 | 562.868374 | 588.018042 | 625.315236 |
| 3    | 40      | AUT  | Austria    | 454.845948 | 484.289226 | 505.740172 | 523.975504 | 540.839652 | 557.432816 | 574.091294 | 595.242932 | 622.995258 |
| 4    | 48      | BHR  | Bahrain    | 363.215052 | 404.889618 | 435.787068 | 461.381678 | 484.361408 | 506.079582 | 527.324116 | 552.986836 | 588.215796 |

If we want to aggregate our data based on other columns, we create a smaller dataframe consisting of relevant PV columns, country identifier (idcntry) and weights column (totwgt). If we want to group our data by gender we simply add itsex to the 't' df: 

```python
t=df[cols_math+cols_science+['totwgt','idcntry','country','iso','itsex']]

# Function for calculating weighted averages. It uses a defined list of columns (`cols`) and pandas series of weights (column `totwgt`). 
# We group our df by columns defined in gr_cols and apply a numpy weighted average function
# We add 'itsex' to our gr_cols list
gr_cols = ['idcntry','iso','country','itsex']
wtavg = lambda x: np.average(x[cols], weights = x['totwgt'], axis = 0)
# math
cols=cols_math
dfwavg_m=t.groupby(gr_cols).apply(wtavg)
# science
cols=cols_science
dfwavg_s=t.groupby(gr_cols).apply(wtavg)

# For each country we get a list of PV values. dfwavg_m contains PVs for math, and dfwavg_s for science
dfwavg_m.head()
```

```python
idcntry  iso  country     itsex 
8        ALB  Albania     Female    [491.5055326915043, 491.86051182957874, 494.11...
                          Male      [495.059478436772, 495.0106249752358, 495.3207...
31       AZE  Azerbaijan  Female    [517.7037901060469, 517.357448265775, 517.8398...
                          Male      [514.7893426996824, 513.6338805605996, 514.112...
36       AUS  Australia   Female    [510.62389959145474, 511.2622975725466, 512.18...
dtype: object
```

The remaining operations - reshaping from wide to long format, saving output file - are the same as in the basic scenario described above.

<div style="page-break-after: always"></div>

# ③ Data Visualization

Data visualizations and pivot table UI are created using Javascript: C3.js and Plotly.

Fig. 2. Snapshot of the Educational Data Analytics dashboard: TIMSS 2019. 

<img src="./img/timss_dash.png" width="900" align=center />

Module 1 is based on open source PivotTable.js - a Javascript Pivot Table library with drag'n'drop functionality built on top of jQuery/jQueryUI and originally written in CoffeeScript.

PivotTable.js' basic function is to enable data exploration and analysis by turning a data set into a summary table and then  adding a drag'n'drop UI to allow users to manipulate this summary table, turning it into a pivot table, very similar in the mechanics to the one found in the popular Excel spreadsheet software but with a bunch of extra developer-oriented features and visualization effects. With optional add-ons, the summary table can be rendered as various kinds of charts, turning the pivot table into a pivot chart (see: [documentation](https://github.com/nicolaskruchten/pivottable/wiki/Frequently-Asked-Questions)). Developer can choose between a few renderers. For this project we have chosen C3.js visualizations because of their superior performance. However, we might change it to Plotly for visual consistency with Modules 2-4.

Main characteristics of PivotTable.js:

- it is light: the core (without chart support) is a single file with less than 1000 lines of code of CoffeeScript, compiles down to 6.3kb of  Javascript minified and gzipped, and depends only on jQuery and jQueryUI's 'sortable'
- it works in projects in which jQuery and jQueryUI work (tested with jQuery 1.8.3 and jQueryUI 1.9.2)
- it works fast in popular browsers on up to around a hundred thousand records, depending on the cardinality of the  attributes.
- its UI is localizable making translations easy and straightforward.
- it works with common data formats (e.g. csv, tsv).
- it has built-in support for basic heatmap and bar chart renderers, and [optional extra renderers that add charting or TSV export support
- its extension points allow aggregation functions, table output, UI and visualizations to be tailored to specific applications
- it works on mobile devices with jQuery UI Touch Punch

PivotTable.js implements a pivot table drag'n'drop UI similar to that found in popular spreadsheet programs. You can drag attributes into/out of the row/column areas, and specify filtering options. 

In order to load the scripts you need to load the dependencies:

1. jQuery in all cases
2. jQueryUI for the interactive `pivotUI()` function
3. D3.js, C3.js
4. load the PivotTable.js files: `pivot.min.js`

## Basic PivotTable.js tutorial

There are two main functions provided by PivotTable.js: `pivot()` and `pivotUI()`, both implemented as jQuery plugins.

`pivot()` creates a static pivotable without UI.

`pivoutUI` yields the same table with a drag'n'drop UI around it

Once you've loaded jQueryUI and pivot.js, the following code:

```javascript
$(function(){
								var derivers = $.pivotUtilities.derivers;
								var renderers = $.extend(
										$.pivotUtilities.renderers,
										$.pivotUtilities.c3_renderers,
										$.pivotUtilities.d3_renderers,
					 					$.pivotUtilities.plotly_renderers,
										$.pivotUtilities.export_renderers
										);
								Papa.parse("./data/TIMSS/timss_means.csv", {
										download: true,
										skipEmptyLines: true,
										complete: function(parsed){
												$("#output").pivotUI(parsed.data, {
													hiddenAttributes: [""],
												  hiddenFromDragDrop: ["score","iso","idcntry"],
												  hiddenFromAggregators: [""],
												  renderers: renderers,
												  aggregators: {
												 "Mean": $.pivotUtilities.aggregators['Average']
												 },
												 renderers: {
										"Table": $.pivotUtilities.renderers['Table']
										, "Table Barchart": $.pivotUtilities.renderers['Table Barchart']
										, "Heatmap": $.pivotUtilities.renderers['Heatmap']
										, "Bar Chart": $.pivotUtilities.renderers['Bar Chart']
										, "Horizontal Bar Chart": $.pivotUtilities.renderers['Horizontal Bar Chart']
										, "Row Data Export": $.pivotUtilities.renderers['TSV Export']
										},
                          	rendererOptions: {
                                  plotly: {
                                     size: { height:600, width:1000}
                                  }
                               },
                               rendererName: "Bar Chart",
                               rowOrder: "value_a_to_z", colOrder: "value_a_to_z",
                               cols: ["variable"],
                               rows: ["country"],
                               aggregatorName: "Mean",
                               vals: ["score"],
                               "inclusions": {
                                 "country": ["Saudi Arabia", "Philippines", "Poland", "Bahrain","England"],
                                 "variable": ["math","science"]
                               },
                                                  });
                                              }
                                          });
                                        });
```

appends the below table to the html id attribute named `$("#output")`:

<img src="./img/table0.png" width="900" align=center />

Please note that the above code refers to the functions described in the file `pivot.js` which creates the "mechanics" of the drag'n'drop functionality. This file is 2000 lines long. For brevity reasons we do not describe it in details in this reports. 

Dragging and dropping variables is enabled by jQuery UI [sortable]('https://jqueryui.com/sortable/') open source library. Variables filtering is delivered using jQuery.  

# ④ User Interface

UI is currently provided by a bootstrap [website](https://etecsa.gitlab.io/dash/). 

Fig. 3. Main site of the Educational Data Analytics.

<img src="./img/index.png" width="900" align=center />

Fig. 4. Analyses selection. 

<img src="./img/select.png" width="900" align=center />

## Project's repository

We host project's repository on Gitlab (https://gitlab.com/etecsa/dash-dev), one of the major open DevOps platform - a lifecycle software tool with a [Git](https://en.wikipedia.org/wiki/Git)-[repository](https://en.wikipedia.org/wiki/Repository_(version_control)) manager providing [wiki](https://en.wikipedia.org/wiki/Wiki), [issue-tracking](https://en.wikipedia.org/wiki/Issue_tracking_system) and [continuous integration](https://en.wikipedia.org/wiki/Continuous_integration) and [deployment](https://en.wikipedia.org/wiki/Continuous_deployment) pipeline.

Repository is private, that is why its visibility is restricted solely to developers added to the project. In order to be added to the project team and gain access to the repository developers should send their gitlab account name at m.k.palinski@gmail.com. Fig. 5. presents main folders and files stored in the repository. 

<div style="page-break-after: always"></div>

Fig. 5. Repository structure.

<img src="./img/repository_structure.png" width="300" align=left />

# Server requirements 

- Implemented pivot table is designed to be a browser-side component, and requires all the data that it is to operate on to be supplied up front in the browser
- No AJAX-style interactions are possible once loaded
- The performance of the app will depend on the machine/browser being used
- App is designed to perform quick calculations on datasets up to 100k rows 
- TIMSS dataset used in the means module consists of 97k rows
- Pivot functionality is quick, but initial data load is a bottleneck
- Equivalent of standard Amazon S3 server or Heroku Production plan should suffice to deal with the app requirements
- Optimal server instance specifications: 8GB DDR RAM, Intel core i7 or equivalent, Network bandwidth up to 10 Gbps
- At this point app does not need to communicate with the server (static hosting is sufficient)
- However, Module II (correlations) requires Flask server callbacks ([Dash app](https://dash.plotly.com/))

# Module 2: Correlations

Module 2 enables user to easily compare variables from TIMSS assessment using correlation (scatter chart) and simple OLS regression. It can be further extended to incorporate data from PISA. The module generates two outputs:

1. scatter chart with a trend line calculated with a use of OLS regression,
2. OLS Regression Results.

Module 2 is created using Dash open-source environment. Dash is a state-of-the-art Python framework for building web analytic applications. Written on top of Flask, Plotly.js, and React.js, Dash is ideal for building data visualization apps with highly custom user interfaces in pure Python. 

Dash apps are rendered in the web browser. Since Dash apps are viewed in the web browser, Dash is inherently cross-platform and mobile ready (see: Dash [documentation](https://dash.plotly.com/introduction)).

<div style="page-break-after: always"></div>

Fig. 6 An overview of the Module 2 structure.

<img src="./img/module2_structure.png" width="800" align=center />

Module 2 enables data exploration on three levels of aggregation:

1. Country
2. School 
3. Student

<div style="page-break-after: always"></div>

Fig. 7. Module II: Correlations - main site.

<img src="./img/module2.png" width="800" align=center />



Each aggregation level provides a similar UI. Users can choose variables to be presented on x and y axis. Produced graphs  are enriched with marginal histograms. Country level additionally groups results into groups of European, Asian and other countries. 

<div style="page-break-after: always"></div>

Fig. 8. Country level.

<img src="./img/country_level.png" width="800" align=left />

<div style="page-break-after: always"></div>

Fig. 9. School level

<img src="./img/school_level.png" width="800" align=left />

<div style="page-break-after: always"></div>

Fig. 10. Student level.

<img src="./img/student_level.png" width="1000" align=left />

<div style="page-break-after: always"></div>

Fig. 11. Additional statistics.

<img src="./img/reg.png" width="700" align=left />



## Tech stack required

<u>Data analysis programmes/languages</u>

<img src="./img/stata.png" width="50" align=left /> 

Stata 14 or higher

**Stata** is a general-purpose statistical software package developed by StataCorp for data manipulation,  visualization, statistics, and automated reporting. It is used by  researchers in many fields such as economics and sociology.

<img src="./img/python.png" width="50" align=left> 

Python 3.6 or higher

**Python** is an interpreted general-purpose programming language. Python's design philosophy emphasizes code readability with its notable use of [significant indentation](https://en.wikipedia.org/wiki/Off-side_rule). Its language constructs as well as its [object-oriented](https://en.wikipedia.org/wiki/Object-oriented_programming) approach aim to help programmers write clear, logical code for small and large-scale projects. Python consistently ranks as one of the most popular programming languages.

<u>Python libraries used in the project</u>

<img src="./img/numpy.png" width="50" align=left /> 

Numpy 1.18.1

**NumPy** is a library for the Python programming language adding support for large, multi-dimensional arrays and matrices,  along with a large collection of high-level mathematical functions to operate on these arrays. NumPy is open-source software and has many contributors.

<img src="./img/pandas.png" width="50" align=left /> 

Pandas 1.2.2



**Pandas** is a software library written for the Python programming language for data manipulation and analysis. In particular, it offers data structures and operations for manipulating numerical tables and time series. It is free software released under the three-clause BSD license. The name is derived from the term "panel data", an econometrics term for data sets that include observations over multiple time periods for the same individuals. Its name is a play on the phrase "Python data analysis" itself.

<img src="./img/statsmodels.svg" width="50" align=left />  

Statsmodels 0.10.2

**Statsmodels** is a Python package that allows users to explore data, estimate statistical models, and perform statistical tests. An extensive list of descriptive statistics, statistical tests, plotting functions, and result statistics are available for different types of data and each estimator. It complements SciPy's stats module.

Statsmodels is part of the Python scientific stack that is oriented towards data analysis, data science and statistics. Statsmodels is built on top of the numerical libraries NumPy and SciPy, integrates with Pandas for data handling. Graphical functions are based on the Matplotlib library. Statsmodels provides the statistical backend for other Python libraries. Statmodels is free software released under the Modified BSD (3-clause) license. 

<u>Visualizations</u>

<img src="./img/c3js.png" width="50" align=left />  

C3.js

C3 makes it easy to generate D3-based charts by wrapping the code required to construct the entire chart. C3 gives some classes to each element when generating, so you can define a custom style by the class and it's possible to extend the structure  directly by D3.



<img src="./img/plotly.png" width="50" align=left /> 

Plotly

Plotly's Python graphing library makes interactive, publication-quality graphs. Plotly.py is [free and open source](https://plotly.com/python/is-plotly-free) 				and you can [view the source on GitHub](https://github.com/plotly/plotly.py). 	



<img src="./img/jquery.png" width="50" align=left /> 

jQuery

jQuery is a JavaScript library designed to simplify HTML DOM tree traversal and manipulation, as well as event handling, CSS animation, and Ajax. It is free, open-source software using the permissive MIT License. As of May 2019, jQuery is used by 73% of the 10 million most popular websites. Web analysis indicates that it is the most widely deployed JavaScript library by a large margin, having at least 3 to 4 times more usage than any other JavaScript library.



<img src="./img/dash.png" width="150" align=left />

Dash apps give a point-&-click interface to models written in Python. With Dash apps, data scientists and  engineers put complex Python analytics in the hands of business decision makers and operators.



<u>All above descriptions are compiled from the official documentations of the presented software.</u>



​		
