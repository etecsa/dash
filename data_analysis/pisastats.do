clear
use "/Volumes/GoogleDrive/Mój dysk/iapy/in/2019/PISA2018_students.dta" if (cnt=="SAU")


gen cnt_part=""
replace cnt_part="South" if stratum=="SAU0001"
replace cnt_part="South" if stratum=="SAU0002"
replace cnt_part="North" if stratum=="SAU0003"
replace cnt_part="North" if stratum=="SAU0004"
replace cnt_part="West" if stratum=="SAU0005"
replace cnt_part="West" if stratum=="SAU0006"
replace cnt_part="Middle" if stratum=="SAU0007"
replace cnt_part="Middle" if stratum=="SAU0008"
replace cnt_part="Middle" if stratum=="SAU0009"
replace cnt_part="Middle" if stratum=="SAU0010"
replace cnt_part="South" if stratum=="SAU0011"
replace cnt_part="South" if stratum=="SAU0012"
replace cnt_part="East" if stratum=="SAU0013"
replace cnt_part="East" if stratum=="SAU0014"
replace cnt_part="North" if stratum=="SAU0015"
replace cnt_part="North" if stratum=="SAU0016"
replace cnt_part="West" if stratum=="SAU0017"
replace cnt_part="West" if stratum=="SAU0018"
replace cnt_part="North" if stratum=="SAU0019"
replace cnt_part="North" if stratum=="SAU0020"
replace cnt_part="North" if stratum=="SAU0021"
replace cnt_part="North" if stratum=="SAU0022"


rename cnt_part region 

encode region, generate(region_n)

decode repeat, gen(repeat2)
gen repeat3="rep"
replace repeat3="norep" if repeat2 == "Did not repeat a  grade"
encode repeat3, gen(repeat_n)


gen grade2 =""
replace grade2="g7" if st001d01t==7
replace grade2="g8" if st001d01t==8
replace grade2="g9" if st001d01t==9
replace grade2="g10" if st001d01t==10
replace grade2="g11" if st001d01t==11
replace grade2="g12" if st001d01t==12
encode grade2, gen(grade_n)

gen durec="."
replace durec="dur0" if durecec==0
replace durec="dur1" if durecec==1
replace durec="dur2" if durecec==2
replace durec="dur3" if durecec==3
replace durec="dur4" if durecec==4
replace durec="dur5" if durecec==5
replace durec="dur6" if durecec==6
replace durec="dur7" if durecec==7
replace durec="dur8" if durecec==8
encode durec, gen(durec_n)

// egen dummy=group(region repeat3),label
//
// local varlist gfofail hedres homepos cultposs escs ictres undrem metaspam wealth perfeed emosups swbp screaddiff adaptivity workmast screadcomp teachint disclima resilience attlnact stimread joyread eudmo compete mastgoal dirins teachsup
//
// foreach var of local varlist {
// 	pisastats `var', cnt(SAU) stats("mean") save("`var'_all") round(1)
// }




egen dummy=group(region repeat3),label

local varlist read math scie

foreach var of local varlist {
	pisastats, pv("`var'") cnt(SAU) stats("mean") save("`var'_all") round(1)
 }

//gender all
// egen dummy2=group(region repeat_n),label
//
// pisastats, pv(read) over(dummy2) cnt(SAU) stats("mean") save(read_all) round(1)
//

