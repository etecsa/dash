clear all

*** GRADE 4

cd "/Volumes/GoogleDrive/Mój dysk/iapy/in/2019/"

foreach stat in mean {
	
	use T2019_asg_gr4, clear
	drop if idcntry>1000
	collapse (`stat') asbgsec asbgsb asbgdml asbgslm asbgicm asbgssb asbgscm asbghrl asmmat0* asssci0* [aw=totwgt], by(idcntry)
	egen math=rowmean(asmmat0*)
	egen science=rowmean(asssci0*)
	*egen asbgsec=rowmean(asbgsec)
	keep idcntry math science asbgsec asbgsb asbgdml asbgslm asbgicm asbgssb asbgscm asbghrl
	
	rename asbgdml disorder
	rename asbgslm likemath
	rename asbgicm clarity
	rename asbgssb belonging
	rename asbgsb bullying
	rename asbgscm confident
	rename asbghrl homeres
	
	save T2019_gr4_`stat'_results, replace
	

	}
	


*pv, pv(asmmat0*) jkzone(jkzone) jkrep(jkrep) weight(totwgt) jrr timss: mean @pv [aw=@w], over(idcntry)

*pv, pv(asssci0*) jkzone(jkzone) jkrep(jkrep) weight(totwgt) jrr timss: mean @pv [aw=@w], over(idcntry)

*p20 p30 p40 p50 p60 p70 p80 p90 sd
